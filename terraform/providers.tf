terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0"
    }
  }
  backend "http" {
    address        = "https://gitlab.com/api/v4/projects/53215786/terraform/state/demo"
    lock_address   = "https://gitlab.com/api/v4/projects/53215786/terraform/state/demo/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/53215786/terraform/state/demo/lock"
  }
}

provider "aws" {
  region = "us-east-1"
}