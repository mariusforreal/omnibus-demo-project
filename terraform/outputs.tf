output "instance-id" {
  description = "The EC2 instance ID"
  value       = aws_instance.instance.id
}

output "instance-public-dns" {
  description = "The EC2 instance public DNS"
  value       = aws_instance.instance.public_dns
}

output "public_ip" {
  value = aws_instance.instance.public_ip
}

output "private_key" {
  value     = tls_private_key.ssh.private_key_pem
  sensitive = true
}

output "public_key_pem" {
  value = tls_private_key.ssh.public_key_pem
}
